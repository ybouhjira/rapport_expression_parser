#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "arbre.h"
#include "expression.h"

int main()
{
  char expression[100];

  // Lecture de l'expression
  printf("Entrez une expression : ");
  char caractere;
  int indice = 0;
  while(caractere != '\n')
    {
      caractere = getchar();
      if(caractere != ' ' && caractere != '\n')
        expression[indice++] = caractere;
    }
  expression[indice] = '\0';

  // Construction de l'arbre
  Arbre *arbre = Construire_arbre(expression);
  printf("Arbre de l'expression : \n");
  Afficher_arbre(arbre, 0);

  // Calcule du résultat :
  printf("Résultat : %.2f\n\n", Calculer_expression(arbre));

  return 0;
}
