%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Stylish Colored Title Page 
% LaTeX Template
% Version 1.0 (27/12/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Peter Wilson (herries.press@earthlink.net)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
% 
% Instructions for using this template:
% This title page compiles as is. If you wish to include this title page in 
% another document, you will need to copy everything before 
% \begin{document} into the preamble of your document. The title page is
% then included using \titleBC within your document.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{book}

\usepackage[svgnames]{xcolor} % Required to specify font color

\newcommand*{\plogo}{\fbox{$\mathcal{PL}$}} % Generic publisher logo

\usepackage{graphicx} % Required for box manipulation
\usepackage{xltxtra,fontspec,xunicode} %UTF-8
%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\newcommand*{\rotrt}[1]{\rotatebox{90}{#1}} % Command to rotate right 90 degrees
\newcommand*{\rotlft}[1]{\rotatebox{-90}{#1}} % Command to rotate left 90 degrees

\newcommand*{\titleBC}{\begingroup % Create the command for including the title page in the document
\centering % Center all text

\def\CP{\textit{\Large \textbf{Evaluation d'une expression mathématique}}} % Title

\settowidth{\unitlength}{\CP} % Set the width of the curly brackets to the width of the title
{\color{Black}\resizebox*{\unitlength}{\baselineskip}{\rotrt{$\}$}}} \\[\baselineskip] % Print top curly bracket
\textcolor{Black}{\CP} \\[\baselineskip] % Print title
{\color{Black}\Large MODULE : STRUCTURES DE DONNES} \\ % Tagline or further description
{\color{Black}\resizebox*{\unitlength}{\baselineskip}{\rotlft{$\}$}}} % Print bottom curly bracket

\vfill % Whitespace between the title and the author name

{\Large 
      \emph{Réalisé par:} 
     \textbf{\\ Youssef Bouhjira \\
     Mohammed Ayoub El Midaoui}\\
     \vspace*{3\baselineskip}
     \emph{Encadré par:} 
     \textbf{\\ Mr. Bekkhoucha} \\
      % Author name
      }

\vfill % Whitespace between the author name and the publisher logo

\vspace*{3\baselineskip}
\vspace*{3\baselineskip}
\vspace*{3\baselineskip}
\vspace*{3\baselineskip}
{\large
Faculté Des Sciences et Technique de Mohammedia \\
 2013 -- 2014} % Year published

\endgroup}

%----------------------------------------------------------------------------------------
%	BLANK DOCUMENT
%----------------------------------------------------------------------------------------

\begin{document} 

\pagestyle{empty} % Removes page numbers

\titleBC % This command includes the title page

\end{document}
